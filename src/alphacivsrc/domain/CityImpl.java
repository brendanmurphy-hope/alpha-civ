package alphacivsrc.domain;

public class CityImpl implements City{
	
	private int production_total;
	private int production_per_round;
	private int population;
	private Player owner;
	private String unit_production;
	private Position position;
	
	public CityImpl(Player owner, Position position) {
		production_total = 0; 
		production_per_round = 6;
		population = 1;
		this.owner = owner;
		unit_production = GameConstants.ARCHER;
		this.position = position;
	}

	@Override
	public Player getOwner() {
		// TODO Auto-generated method stub
		return owner;
	}
	
	public void setOwner(Player type) {
		// TODO Auto-generated method stub
		owner = type;
	}

	@Override
	public int getSize() {
		// TODO Auto-generated method stub
		return 1;
	}

	@Override
	public String getProduction() {
		// TODO Auto-generated method stub
	   return unit_production;
	}

	@Override
	public String getWorkforceFocus() {
		// TODO Auto-generated method stub
		return null;
	}
	
	public int get_production_per_round() {
		return production_per_round;
	}
	
	public int getProductionTotal() {
		return production_total;
	}
	
	public void setProductionTotal() {
		production_total = production_per_round + production_total;
	}

	public Object getPopulation() {
		// TODO Auto-generated method stub
		return population;
	}

	
	public void setProductionType(String type_to_produce) {
		unit_production = type_to_produce;
	}
	
	public Position getPosition() {
		return position;
	}
}
