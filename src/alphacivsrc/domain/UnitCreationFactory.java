package alphacivsrc.domain;

public interface UnitCreationFactory {
	UnitImpl createUnit(Player owner, String unitType);
}
