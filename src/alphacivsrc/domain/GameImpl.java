package alphacivsrc.domain;

import java.util.ArrayList;

/**
 * Skeleton implementation of HotCiv.
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Computer Science Department Aarhus University
 * 
 * This source code is provided WITHOUT ANY WARRANTY either expressed or
 * implied. You may study, use, modify, and distribute it for non-commercial
 * purposes. For any commercial use, see http://www.baerbak.com/
 */

public class GameImpl implements Game {
	private TileImpl[][] boardArray;
	private Player playerTurn;
	private ArrayList<CityImpl> cities;
	private ArrayList<UnitImpl> units;
	private int age;
	private Player winner;
	private Aging agingStrategy;
	private WinStrategy winStrategy;
	private UnitCreationFactory unitFactory;

	public GameImpl(Aging agingStrategy, WinStrategy winStrategy, BoardSetUp boardSetUp,
			UnitCreationFactory unitFactory) {
		playerTurn = Player.RED;
		age = -4000;

		this.unitFactory = unitFactory;

		boardArray = boardSetUp.setUpBoard(this.unitFactory);
		cities = boardSetUp.getCities();
		units = boardSetUp.getUnits();

		this.agingStrategy = agingStrategy;
		this.winStrategy = winStrategy;
	}

	public ArrayList<CityImpl> getCities() {
		return cities;
	}

	public Tile getTileAt(Position p) {
		return boardArray[p.getRow()][p.getColumn()];
	}

	public Unit getUnitAt(Position p) {
		return boardArray[p.getRow()][p.getColumn()].getUnit();
	}

	public TileImpl[][] getBoardArray() {
		return boardArray;
	}

	public City getCityAt(Position p) {
		return boardArray[p.getRow()][p.getColumn()].getCity();
	}

	public Player getPlayerInTurn() {
		return playerTurn;
	}

	public Player getWinner() {
		return winner;
	}

	public int getAge() {
		return age;
	}

	private boolean boundariesValid(Position to) {
		if (to.getRow() >= GameConstants.WORLDSIZE || to.getColumn() >= GameConstants.WORLDSIZE) {
			return false;
		}
		if (to.getRow() <= -1 || to.getColumn() <= -1) {
			return false;
		}
		return true;
	}

	private boolean moveIsValid(Position from, Position to) {
		UnitImpl unit = (UnitImpl) getUnitAt(new Position(from.r, from.getColumn()));
		if (Math.abs(from.getRow() - to.getRow()) > unit.getMoveCount()
				|| Math.abs(from.getColumn() - to.getColumn()) > unit.getMoveCount()) {
			return false;
		}
		return true;
	}

	private boolean isUnitThere(Position from) {
		if (boardArray[from.getRow()][from.getColumn()].getUnit() == null) {
			return false;
		}
		return true;
	}

	private boolean isPlayersUnit(Unit unitFrom) {
		if (!playerTurn.equals(unitFrom.getOwner())) {
			return false;
		}
		return true;
	}

	public boolean moveUnit(Position from, Position to) {
		if (!boundariesValid(to)) {
			return false;
		}

		UnitImpl unitFrom;
		if (!isUnitThere(from)) {
			return false;
		}

		if (!moveIsValid(from, to)) {
			return false;
		}

		unitFrom = boardArray[from.getRow()][from.getColumn()].getUnit();
		UnitImpl unitTo = boardArray[to.getRow()][to.getColumn()].getUnit();

		String typeOfTileMovingTo = boardArray[to.getRow()][to.getColumn()].getTypeString();
		if (!isPlayersUnit(unitFrom)) {
			return false;
		}

		if (typeOfTileMovingTo.equals(GameConstants.MOUNTAINS) || typeOfTileMovingTo.equals(GameConstants.OCEANS)) {
			return false;
		}

		if (unitTo != null && unitFrom.getOwner().equals(unitTo.getOwner())) {
			return false;
		}

		// Everything checks out, now the unit moving process begins
		City city = boardArray[to.getRow()][to.getColumn()].getCity();
		if (city != null && !unitFrom.getOwner().equals(city.getOwner())) {
			// Conquer city if unit moves onto enemy city
			((CityImpl) city).setOwner(unitFrom.getOwner());
			checkWinner();
		}
		// Actually moves unit
		boardArray[to.getRow()][to.getColumn()].setUnit(boardArray[from.getRow()][from.getColumn()].getUnit());
		boardArray[to.getRow()][to.getColumn()].getUnit().setMovedThisTurn(true);
		// delete old unit
		boardArray[from.getRow()][from.getColumn()].setUnit(null);
		return true;
	}

	private void checkWinner() {
		winner = winStrategy.checkForWinner(age, cities, playerTurn);
	}

	public void endOfTurn() {
		if (playerTurn.equals(Player.RED)) {
			playerTurn = Player.BLUE;
		} else {
			playerTurn = Player.RED;
			for (CityImpl city : cities) {
				city.setProductionTotal();
				cityProductionCheck(city.getPosition());
			}
			for (UnitImpl unit : units) {
				unit.setMovedThisTurn(false);
			}
			age = agingStrategy.calculateAge(age);
			checkWinner();
		}
	}

	public void setAge(int age) {
		this.age = age;
	}

	public void setWinner() {
		winner = Player.RED;
	}

	public void cityProductionCheck(Position p) {
		CityImpl city = (CityImpl) boardArray[p.getRow()][p.getColumn()].getCity();
		if (city.getProduction().equals(GameConstants.ARCHER)
				&& city.getProductionTotal() >= GameConstants.ARCHERCOST) {
			UnitImpl unitToPlace = unitFactory.createUnit(playerTurn, GameConstants.ARCHER);
			placeUnitAfterProduction(p, unitToPlace);
		} else if (city.getProduction().equals(GameConstants.LEGION)
				&& city.getProductionTotal() >= GameConstants.LEGIONCOST) {
			UnitImpl unitToPlace = unitFactory.createUnit(playerTurn, GameConstants.LEGION);
			placeUnitAfterProduction(p, unitToPlace);
		} else if (city.getProduction().equals(GameConstants.SETTLER)
				&& city.getProductionTotal() >= GameConstants.SETTLERCOST) {
			UnitImpl unitToPlace = unitFactory.createUnit(playerTurn, GameConstants.SETTLER);
			placeUnitAfterProduction(p, unitToPlace);
		}
	}

	public boolean placeUnitAfterProduction(Position p, UnitImpl unit) {
		TileImpl tileToCheck = null;
		int row = p.getRow();
		int col = p.getColumn();
		for (int i = 0; i < 9; i++) {
			switch (i) {
			case 0:
				tileToCheck = boardArray[row][col];
				break;
			case 1:
				if (row - 1 >= 0)
					tileToCheck = boardArray[row - 1][col];
				break;
			case 2:
				if (row - 1 >= 0 && col + 1 < GameConstants.WORLDSIZE)
					tileToCheck = boardArray[row - 1][col + 1];
				break;
			case 3:
				if (col + 1 < GameConstants.WORLDSIZE)
					tileToCheck = boardArray[row][col + 1];
				break;
			case 4:
				if (row + 1 < GameConstants.WORLDSIZE || col + 1 < GameConstants.WORLDSIZE)
					tileToCheck = boardArray[row + 1][col + 1];
				break;
			case 5:
				if (row + 1 < GameConstants.WORLDSIZE)
					tileToCheck = boardArray[row + 1][col];
				break;
			case 6:
				if (row + 1 < GameConstants.WORLDSIZE || col - 1 >= 0)
					tileToCheck = boardArray[row + 1][col - 1];
				break;
			case 7:
				if (col - 1 >= 0)
					tileToCheck = boardArray[row][col - 1];
				break;
			case 8:
				if (row - 1 >= 0 || col - 1 >= 0)
					tileToCheck = boardArray[row - 1][col - 1];
				break;
			}
			if (tileToCheck.getUnit() != null || !tileToCheck.isOccupiable()) {
				continue;
			} else {
				units.add(unit);
				tileToCheck.setUnit(unit);
				return true;
			}
		}
		return false;
	}

	public void changeWorkForceFocusInCityAt(Position p, String balance) {
	}

	public void changeProductionInCityAt(Position p, String unitType) {

	}

	public void performUnitActionAt(Position p) {
		UnitContext unitContext = new UnitContext((TileImpl) getTileAt(p));
		UnitImpl unit = (UnitImpl) getUnitAt(p);
		if (unit != null && unit.getOwner().equals(playerTurn)) {
			UnitActions action = unit.getUnitAction();
			if (action != null) {
				action.unitPreformsAction(unitContext);
			}
		}

	}
}
