package alphacivsrc.domain;

public class TileImpl implements Tile {

	private Position position;
	private String type;
	private UnitImpl unit;
	private CityImpl city;
	private boolean occupiable;

	public TileImpl(Position pos, String type) {
		position = pos;
		this.type = type;
		unit = null;
		city = null;
		occupiable = isOccupiable();
	}

	@Override
	public Position getPosition() {
		return position;
	}

	@Override
	public String getTypeString() {
		return type;
	}

	public void setUnit(UnitImpl unit) {
		this.unit = unit;
	}

	public UnitImpl getUnit() {
		return unit;
	}

	public void setCity(CityImpl city) {
		this.city = city;
	}

	public City getCity() {
		return city;
	}

	public boolean isOccupiable() {
		if (type.equals(GameConstants.OCEANS) || type.equals(GameConstants.MOUNTAINS)) {
			return false; }
		return true;
	}

}
