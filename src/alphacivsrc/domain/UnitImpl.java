package alphacivsrc.domain;

public class UnitImpl implements Unit {

	private Player owner;
	private String type;
	private int production_cost;
	private boolean moved;
	private int moveCount;
	private UnitActions unitAction;
	private int defense;

	public UnitImpl(Player player, String type, UnitActions unitAction) {
		this.owner = player;
		this.type = type;
		costSetter();
		moved = false;
		moveCount = 1;
		this.setUnitAction(unitAction);
		defense = 3;
	}

	@Override
	public String getTypeString() {
		return type;
	}

	@Override
	public Player getOwner() {
		return owner;
	}

	@Override
	public int getMoveCount() {
		return moveCount;
	}
	
	public void setMoveCount(int moveCount) {
		this.moveCount = moveCount;
	}

	@Override
	public int getDefensiveStrength() {
		return defense;
	}
	
	public void setDefensiveStrength(int defense) {
		this.defense = defense;
	}

	@Override
	public int getAttackingStrength() {
		return 0;
	}
	
	public void costSetter() {
		if(type.equals(GameConstants.ARCHER)) {
			production_cost = 10;
		}
		else if(type.equals(GameConstants.LEGION)) {
			production_cost = 15;
		}
		else {
			production_cost = 30;
		}
	}

	public boolean hasMovedThisTurn() {
		return moved;
	}
	
	public void setMovedThisTurn (boolean moved) {
		this.moved = moved;
	}

	public UnitActions getUnitAction() {
		return unitAction;
	}

	public void setUnitAction(UnitActions unitAction) {
		this.unitAction = unitAction;
	}

}
