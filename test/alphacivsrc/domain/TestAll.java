package alphacivsrc.domain;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
	@Suite.SuiteClasses(
			{TestAlphaCiv.class, TestBetaCiv.class, TestLinearAging.class, 
				TestProgressiveAging.class, TestAlphaCivWinStrat.class,
				TestBetaCivWinStrat.class, TestArcherAction.class,
				TestSettlerAction.class, TestGammaCiv.class,
				TestDeltaCiv.class}

			)



public class TestAll { //Dummy

}
