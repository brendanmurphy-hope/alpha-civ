package alphacivsrc.domain;

import org.junit.*;
import static org.junit.Assert.*;

/**
 * Skeleton class for AlphaCiv test cases
 * 
 * This source code is from the book "Flexible, Reliable Software: Using
 * Patterns and Agile Development" published 2010 by CRC Press. Author: Henrik B
 * Christensen Computer Science Department Aarhus University
 * 
 * This source code is provided WITHOUT ANY WARRANTY either expressed or
 * implied. You may study, use, modify, and distribute it for non-commercial
 * purposes. For any commercial use, see http://www.baerbak.com/
 */
public class TestAlphaCiv {
	private Game game;
	private TileImpl[][] boardArray;

	/** Fixture for alphaciv testing. */
	@Before
	public void setUp() {
		game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp(), new UnitCreationAlphaCiv());

		boardArray = ((GameImpl) game).getBoardArray();
	}

	@Test
	public void shouldHaveRedCityAt1_1() {
		City c = game.getCityAt(new Position(1, 1));
		assertNotNull("There should be a city at (1,1)", c);
		Player p = c.getOwner();
		assertEquals("City at (1,1) should be owned by red", Player.RED, p);
	}
	
	@Test
	public void shouldHaveBlueCityAt4_1() {
		City c = game.getCityAt(new Position(4, 1));
		assertNotNull("There should be a city at (4, 1)", c);
		Player p = c.getOwner();
		assertEquals("City at (4,1) should be owned by blue", Player.BLUE, p);
	}
	
	@Test
	public void redArcherStartAtTheRightSpot() {
		Unit redArcher = game.getUnitAt(new Position(2,0));
		assertNotNull("There should be a unit at (2, 0)", redArcher);
		Player p = redArcher.getOwner();
		assertEquals("This unit is owned by red", Player.RED, p);
		String type = redArcher.getTypeString();
		assertEquals("This unit is an archer", GameConstants.ARCHER, type);
	}
	
	@Test
	public void redSettlerStartAtTheRightSpot() {
		Unit redSettler = game.getUnitAt(new Position(4,3));
		assertNotNull("There should be a unit at (4, 3)", redSettler);
		Player p = redSettler.getOwner();
		assertEquals("This unit is owned by blue", Player.RED, p);
		String type = redSettler.getTypeString();
		assertEquals("This unit is an legion", GameConstants.SETTLER, type);
	}
	
	@Test
	public void testOceanIsAt1_0() {
		Tile tile = game.getTileAt(new Position(1,0));
		assertNotNull("There should be a tile", tile);
		String type = tile.getTypeString();
		assertEquals("This should be an ocean", GameConstants.OCEANS, type);
	}
	
	@Test
	public void testHillsIsAt0_1() {
		Tile tile = game.getTileAt(new Position(0,1));
		assertNotNull("There should be a tile", tile);
		String type = tile.getTypeString();
		assertEquals("This should be a hill", GameConstants.HILLS, type);
	}
	
	@Test
	public void testMountainsIsAt2_2() {
		Tile tile = game.getTileAt(new Position(2,2));
		assertNotNull("There should be a tile", tile);
		String type = tile.getTypeString();
		assertEquals("This should be a mountains", GameConstants.MOUNTAINS, type);
	}
	
	@Test
	public void testPlainsIsAt15_15() {
		Tile tile = game.getTileAt(new Position(15,15));
		assertNotNull("There should be a tile", tile);
		String type = tile.getTypeString();
		assertEquals("This should be a plain", GameConstants.PLAINS, type);
	}
	
	@Test
	public void blueLegionStartAtTheRightSpot() {
		Unit blueLegion = game.getUnitAt(new Position(3,2));
		assertNotNull("There should be a unit at (3, 2)", blueLegion);
		Player p = blueLegion.getOwner();
		assertEquals("This unit is owned by blue", Player.BLUE, p);
		String type = blueLegion.getTypeString();
		assertEquals("This unit is an legion", GameConstants.LEGION, type);
	}

	@Test
	public void redGoesFirst() {
		assertEquals("Red goes first ", Player.RED, game.getPlayerInTurn());
	}


	@Test
	public void unitExists() {
		Unit unit = game.getUnitAt(new Position(2, 0));
		assertNotNull("Unit at (2,0) should be there ", unit);
		Player P = unit.getOwner();
		assertEquals(" The unit should belong to red", Player.RED, P);
	}

	@Test
	public void noMovementOnMountains() {
		UnitImpl unit = (UnitImpl) game.getUnitAt(new Position(3, 2));
		assertNotNull("Unit at (3,2) should be there ", unit);
		assertFalse(game.moveUnit(new Position(3, 2), new Position(2, 2)));
	}
	
	@Test
	public void noMovementOnOcean() {
		Unit unit = game.getUnitAt(new Position(2,0));
		assertNotNull("Unit at (2,0) should be there ", unit);
		Tile tile = game.getTileAt(new Position(1, 0));
		assertEquals("This should be an ocean", GameConstants.OCEANS, tile.getTypeString());
		assertFalse(game.moveUnit(new Position(2,0), new Position(1,0)));
	}
	
	@Test
	public void testNoMovementOffBoard() {
		assertFalse(game.moveUnit(new Position(2,0), new Position(2,-1)));
	}
	
	@Test
	public void testNoMovementOffBoardOtherEnd() {
		assertFalse(game.moveUnit(new Position(15,15), new Position(15,16)));
	}
	
	@Test
	public void noMovementOfMoreThanOneSpace() {
		Unit unit = game.getUnitAt(new Position(3, 2));
		assertNotNull("Unit at (3,2) should be there ", unit);
		assertFalse(game.moveUnit(new Position(3, 2), new Position(2, 4)));
		assertFalse(game.moveUnit(new Position(3, 2), new Position(5, 2)));
	}

	@Test
	public void redCannotMoveBlueUnits() {
		redGoesFirst();
		UnitImpl unit = new UnitImpl(Player.BLUE, GameConstants.ARCHER, new ArcherActionImpl());
		Player p = unit.getOwner();
		assertEquals("This should unit should be blue", Player.BLUE, p);

		TileImpl tile = new TileImpl(new Position(4, 4), GameConstants.PLAINS);
		tile.setUnit(unit);
		boardArray[4][4] = tile;
		assertFalse(game.moveUnit(new Position(4, 4), new Position(5, 4)));
	}
	
	@Test
	public void noMovementMoreThanOncePerTurn() {
		UnitImpl redArcher = boardArray[2][0].getUnit();
		game.moveUnit(new Position(2,0), new Position(2,1));
		assertTrue("this unit has moved this turn ", redArcher.hasMovedThisTurn());
	}
	
	@Test
	public void movementRestoredAfterRound() {
		game.moveUnit(new Position(2,0), new Position(2,1));
		UnitImpl redArcher = boardArray[2][1].getUnit();
		game.moveUnit(new Position(4,3), new Position(4,4));
		UnitImpl redSettler = boardArray[4][4].getUnit();
		assertTrue("this unit has moved this turn ", redArcher.hasMovedThisTurn());
		assertTrue("this unit has moved this turn ", redSettler.hasMovedThisTurn());
		game.endOfTurn();
		game.moveUnit(new Position(3,2), new Position(3,3));
		UnitImpl blueLegion = boardArray[3][3].getUnit();
		assertTrue("this unit has moved this turn ", blueLegion.hasMovedThisTurn());
		game.endOfTurn();
		assertFalse("this unit can move again ", redArcher.hasMovedThisTurn());
		assertFalse("this unit can move again ", redSettler.hasMovedThisTurn());
		assertFalse("this unit can move again ", blueLegion.hasMovedThisTurn());		
	}

	@Test
	public void sixAdditionalProductionPerTurn() {
		CityImpl city = (CityImpl) ((GameImpl) game).getCities().get(0);
		assertEquals("we produce six production every turn", city.get_production_per_round(), 6);

	}

	@Test
	public void production_total_increments_after_2_turns() {
		;
		CityImpl city = (CityImpl) ((GameImpl) game).getCities().get(0);
		TileImpl tile = new TileImpl(new Position(1, 1), GameConstants.PLAINS);
		tile.setCity(city);
		assertEquals(0, city.getProductionTotal());
		game.endOfTurn();
		game.endOfTurn();
		assertEquals(6, city.getProductionTotal());

	}

	@Test
	public void cityPopulationIs1() {
		CityImpl city = (CityImpl) ((GameImpl) game).getCities().get(0);
		TileImpl tile = new TileImpl(new Position(1, 1), GameConstants.PLAINS);
		tile.setCity(city);
		assertEquals(1, city.getPopulation());
	}

	@Test
	public void afterRedBlueAfterBlueRed() {
	//	game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp());
		game.endOfTurn();
		assertEquals(game.getPlayerInTurn(), Player.BLUE);
		game.endOfTurn();
		assertEquals(game.getPlayerInTurn(), Player.RED);
	}

	@Test
	public void roundAdvancesAge() {
	//	game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp());
		game.endOfTurn();
		game.endOfTurn();
		assertEquals(game.getAge(), -3900);
	}

	@Test
	public void advancingAge() {
	//	game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp());
		while (game.getAge() != -3000) {
			game.endOfTurn();
			game.endOfTurn();
		}
		assertEquals(game.getAge(), -3000);
	}

	@Test
	public void redWins() {
	//	game = new GameImpl(new LinearAgingImpl(), new AlphaCivWinStrat(), new AlphaCivSetUp());
		while (game.getAge() != -3000) {
			game.endOfTurn();
			game.endOfTurn();
		}
		assertEquals(game.getWinner(), Player.RED);
	}

	@Test
	public void redUnitDestroysBlueUnit() {
		UnitImpl unit = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		UnitImpl unitB = new UnitImpl(Player.BLUE, GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = new TileImpl(new Position(5, 4), GameConstants.PLAINS);
		TileImpl tileB = new TileImpl(new Position(5, 5), GameConstants.PLAINS);
		tile.setUnit(unit);
		tileB.setUnit(unitB);
		boardArray[5][4] = tile;
		boardArray[5][5] = tileB;
		game.moveUnit(new Position(5, 4), new Position(5, 5));
		assertEquals(unit, game.getUnitAt(new Position(5, 5)));
		assertFalse(unitB.equals(game.getUnitAt(new Position(5, 5))));
	}

	@Test
	public void noUnitsOnSameTile() {
		UnitImpl unit = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		UnitImpl unitB = new UnitImpl(Player.RED, GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = new TileImpl(new Position(5, 4), GameConstants.PLAINS);
		TileImpl tileB = new TileImpl(new Position(5, 5), GameConstants.PLAINS);
		tile.setUnit(unit);
		tileB.setUnit(unitB);
		boardArray[5][4] = tile;
		boardArray[5][5] = tileB;
		assertFalse("This move should not be allowed", game.moveUnit(new Position(5,4),  new Position(5,5)));
	}
	
	@Test
	public void cantMoveNullUnit() {
		assertFalse("This move should not be allowed", game.moveUnit(new Position(5,4),  new Position(5,5)));
	}
	
	@Test
	public void buildAUnit() {
		Unit unit = game.getUnitAt(new Position(1,1));
		assertNull("There should be no unit at (1,1)", unit);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		Unit unitNow = game.getUnitAt(new Position(1,1));
		assertNotNull("There is now a unit here after production was high enough ", unitNow);
	}
	
	@Test
	public void testSwitchingProduction() {
		CityImpl city = (CityImpl) ((GameImpl) game).getCities().get(0);
		assertEquals("We are producing an archer by default", city.getProduction(), GameConstants.ARCHER);
		game.endOfTurn();
		game.endOfTurn();
		//6
		city.setProductionType(GameConstants.LEGION);
		assertEquals("we are now producing a legion ", city.getProduction(), GameConstants.LEGION);
		game.endOfTurn();
		game.endOfTurn();
		//12
		city.setProductionType(GameConstants.SETTLER);
		assertEquals("we are now producing a Settler ", city.getProduction(), GameConstants.SETTLER);
		game.endOfTurn();
		game.endOfTurn();
		//18
		game.endOfTurn();
		game.endOfTurn();
		//24
		game.endOfTurn();
		game.endOfTurn();
		TileImpl checkForSettler = boardArray[city.getPosition().getRow()][city.getPosition().getColumn()];
		assertEquals("there is now a settler here ", GameConstants.SETTLER,  checkForSettler.getUnit().getTypeString());
	}
	
	@Test
	public void buildClockwiseSlot1() {
		//Slot 1 is at position 3,1
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][1];
		tile.setUnit(unit);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		assertNotNull("There is now a unit here after production was high enough ", game.getUnitAt(new Position(3,1)));
		
	}
	
	@Test
	public void buildClockwiseSlot3() {
		//Slot 3 is at position 4,2
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][1];
		tile.setUnit(unit);
		UnitImpl unit1 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile1 = boardArray[3][1];
		tile1.setUnit(unit1);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		assertNotNull("There is now a unit here after production was high enough ", game.getUnitAt(new Position(4,2)));
	}
	
	@Test
	public void buildClockwiseSlot4() {
		//Slot 4 is at position 5,2
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][1];
		tile.setUnit(unit);
		UnitImpl unit1 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile1 = boardArray[3][1];
		tile1.setUnit(unit1);
		UnitImpl unit3 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile3 = boardArray[4][2];
		tile3.setUnit(unit3);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		assertNotNull("There is now a unit here after production was high enough ", game.getUnitAt(new Position(5,2)));
	}
	
	@Test
	public void buildClockwiseSlot5() {
		//Slot 5 is at position 5,1
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][1];
		tile.setUnit(unit);
		UnitImpl unit1 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile1 = boardArray[3][1];
		tile1.setUnit(unit1);
		UnitImpl unit3 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile3 = boardArray[4][2];
		tile3.setUnit(unit3);
		UnitImpl unit4 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile4 = boardArray[5][2];
		tile4.setUnit(unit4);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		assertNotNull("There is now a unit here after production was high enough ", game.getUnitAt(new Position(5,1)));
	}
	
	@Test
	public void buildClockwiseSlot6() {
		//Slot 6 is at position 5,0
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][1];
		tile.setUnit(unit);
		UnitImpl unit1 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile1 = boardArray[3][1];
		tile1.setUnit(unit1);
		UnitImpl unit3 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile3 = boardArray[4][2];
		tile3.setUnit(unit3);
		UnitImpl unit4 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile4 = boardArray[5][2];
		tile4.setUnit(unit4);
		UnitImpl unit5 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile5 = boardArray[5][1];
		tile5.setUnit(unit5);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		assertNotNull("There is now a unit here after production was high enough ", game.getUnitAt(new Position(5,0)));
	}
	
	@Test
	public void buildClockwiseSlot7() {
		//Slot 7 is at position 4,0
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][1];
		tile.setUnit(unit);
		UnitImpl unit1 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile1 = boardArray[3][1];
		tile1.setUnit(unit1);
		UnitImpl unit3 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile3 = boardArray[4][2];
		tile3.setUnit(unit3);
		UnitImpl unit4 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile4 = boardArray[5][2];
		tile4.setUnit(unit4);
		UnitImpl unit5 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile5 = boardArray[5][1];
		tile5.setUnit(unit5);
		UnitImpl unit6 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile6 = boardArray[5][0];
		tile6.setUnit(unit6);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		assertNotNull("There is now a unit here after production was high enough ", game.getUnitAt(new Position(4,0)));
	}
	
	@Test
	public void buildClockwiseSlot8() {
		//Slot 8 is at position 3,0
		game.endOfTurn();
		UnitImpl unit = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile = boardArray[4][1];
		tile.setUnit(unit);
		UnitImpl unit1 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile1 = boardArray[3][1];
		tile1.setUnit(unit1);
		UnitImpl unit3 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile3 = boardArray[4][2];
		tile3.setUnit(unit3);
		UnitImpl unit4 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile4 = boardArray[5][2];
		tile4.setUnit(unit4);
		UnitImpl unit5 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile5 = boardArray[5][1];
		tile5.setUnit(unit5);
		UnitImpl unit6 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile6 = boardArray[5][0];
		tile6.setUnit(unit6);
		UnitImpl unit7 = new UnitImpl(Player.BLUE,GameConstants.ARCHER, new ArcherActionImpl());
		TileImpl tile7 = boardArray[4][0];
		tile7.setUnit(unit7);
		game.endOfTurn();
		game.endOfTurn();
		game.endOfTurn();
		assertNotNull("There is now a unit here after production was high enough ", game.getUnitAt(new Position(3,0)));
	}
	
	@Test
	public void isThisTileAbleToHoldAUnit() {
		TileImpl tileMount = boardArray[2][2];
		assertFalse("tile not occupiable ", tileMount.isOccupiable());
		TileImpl tileOcean = boardArray[1][0];
		assertFalse("tile no occupiable ", tileOcean.isOccupiable());
		TileImpl tilePlains = boardArray[15][15];
		assertTrue("This tile can hold a unit ", tilePlains.isOccupiable());
	}
}